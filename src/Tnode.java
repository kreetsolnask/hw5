/* http://stackoverflow.com/questions/19895308/converting-from-reverse-polish-notationrpn-into-a-tree-form
 * http://enos.itcollege.ee/~jpoial/algoritmid/TreeNode.java
 * https://git.wut.ee/i231/home5/src/master/src/Node.java */

import java.util.*;

public class Tnode implements Iterator<Tnode> {

   private String name;
   private Tnode firstChild;
   private Tnode nextSibling;
   private static Tnode root;

   
   Tnode(String s, Tnode p, Tnode a){
	   setName (s);
	   setNextSibling (p);
	   setFirstChild (a);
   }
	   Tnode() {
	      this ("", null, null);
	   }

	   Tnode (String s) {
	      this (s, null, null);
	   }

	   Tnode (String s, Tnode p) {
	      this (s, p, null);
	   }

   public void setName(String s) {
	   name = s;
   }
   public String getName() {
	   return name;
   }
   public void setNextSibling (Tnode p){
	   nextSibling = p;
   }
   public Tnode getNextSibling () {
	   return nextSibling;
   }
   public void setFirstChild (Tnode a){
	   firstChild = a;
   }
   public Tnode getFirstChild () {
	   return firstChild;
   }
   public boolean hasNext () { /* meetod hasNext() defineeritakse läbi selle, et getNextSibling() ei tohi olla null */
	   return (getNextSibling() != null); 
   }
   public Tnode next(){
	   return getNextSibling();
   }
   public Iterator<Tnode> children() {
	      return getFirstChild();
   }
	      
   public boolean isLeaf () {
	   return (getFirstChild() == null);
   }
   public void preorder () {
	   Iterator<Tnode> children = children();
	      while (children != null) {
	         ((Tnode)children).preorder();
	         children = (Tnode)children.next();
	      }
	   }
  
   @Override
   public String toString() /* peab väljastama vaskpoolse suluesitluse */
   { 
     /* if (root == null) {
    	  throw new NoSuchElementException ("Juur on tühi.");
      }*/
	  StringBuffer sb = new StringBuffer(); /* creates an empty string buffer with the initial capacity of 16*/
	  sb.append(getName()); /*StringBuffer-i meetod append(), mis liidab olemasolevatele sümbolitele veel sümboleid juurde*/
	  if (this.getFirstChild() != null) /*Kui esimene laps on null ehk pole veel esimest last, lisatakse avav sulg */
	  {
		  sb.append("(");
		  sb.append(this.getFirstChild().toString());/*lisatakse esimene laps */  
	  }
	  if (this.getNextSibling() != null) /* Kui järgmine laps on null, lisatakse , */
	  {
		  sb.append(",");
		  sb.append(this.getNextSibling().toString()); /*lisatakse järgmine laps */
		  sb.append(")"); /*kõige lõppu lisatakse lõpetav sulg */
	  }
	  return sb.toString(); /*tagastatakse erinevatest sümbolitest kokku pandud jada */
   	}
   
   public static boolean isValidOperator(String s) { // meetod, mis kontrollib, kas tehe on õige
	   return s.equals("-") || s.equals("+") || s.equals("/") || s.equals("*");
   }
   
   public static Tnode buildFromRPN (String pol) { /*Enamus pärineb kolmandast kodutööst */
	   StringTokenizer st = new StringTokenizer(pol, " \t");
	  int i = 1;
 	  int opcount = 0;
 	  int numcount = 0;
 	  int tcount = st.countTokens(); //loeb tükid üle
 	  
	   if (pol.trim().length()==0){
		   throw new IllegalArgumentException ("Sisestatud avaldis on tühi.");
	   }
	     Stack<Tnode> stack = new Stack<Tnode>(); /* uue magasini loomine*/
	      while(st.hasMoreTokens()) {
	    	  String token = st.nextToken();
	      
	      		if (isValidOperator(token)) 
	      		{
	      			if (stack.size() == 0 || stack.size() == 1 ) {
	      				throw new RuntimeException ("Avaldises: " +pol+ " ei ole piisavalt elemente tehte: " +token+ " teostamiseks.");
	      			}
	      			else 
	      			{
	      			Tnode Tn1 = stack.pop();
	      			Tnode Tn2 = stack.pop();
	      			stack.push(new Tnode(token, Tn1, Tn2));
	      			opcount++;
	      			}
	      		}
	      		else 
	      		{
	      			try {
	      	    		double tmp = Double.parseDouble(token); // kui ei ole tehtemärk, siis kontrollin, et oleks arv.
	      	    		Tnode tn = new Tnode (token);
	      	    		stack.push(tn);
	      	    		numcount++;
	      	    		}
	      	    		catch (RuntimeException e){
	      	    		throw new RuntimeException ("Avaldises: " + pol + " on vigane sümbol: " +token);
	      	    			}
	      			}
	      		}
	      if (numcount - 1 > opcount){
	    	  throw new RuntimeException ("Avaldis " +pol+ " sisaldab liiga palju numbreid.");
	      }
	     /*if (numcount - 1 < opcount){
	    	  throw new RuntimeException ("Avaldis " +pol+ " sisaldab liiga palju tehtemärke.");
	      }*/
	      root = stack.pop();
	      return root;
   }
   public static void main (String[] param) {
	  String s = "2 3 6 - ";
	  System.out.println ("RPN: " + s);
	  Tnode t = Tnode.buildFromRPN (s);
      System.out.println("Trees: " + t.toString());
      
   }
}

